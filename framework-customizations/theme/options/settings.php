<?php if (!defined('FW')) die('Forbidden');

$options = array(
	
	'global_settings' => array(
		'type' => 'tab',
		'options' => array(
		
			'preloader_display' => array(
				'type'  => 'switch',
				'value' => 'enable',
				'label' => esc_html__('Preloader', 'sobre-lite'),
				'desc'  => esc_html__('Preloader Display Settings', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'enable',
					'label' => esc_html__('Enable', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'disable',
					'label' => esc_html__('Disable', 'sobre-lite'),
				),
			),
			'google_api_key' => array(
				'type'  => 'text',
				'label' => esc_html__('API Key', 'sobre-lite'),
				'desc'  => esc_html__('API Key for Google Map', 'sobre-lite'),
			),			
		
		),
		'title' => esc_html__('Global Settings', 'sobre-lite'),
	),	
	
	'header_settings' => array(
		'type' => 'tab',
		'options' => array(
		
			'logo_image' => array(
				'type'  => 'upload',
				'label' => esc_html__('Logo', 'sobre-lite'),
				'desc'  => esc_html__('Upload Image Logo', 'sobre-lite'),
			),			
		
		),
		'title' => esc_html__('Header Settings', 'sobre-lite'),
	),
	'blog_settings' => array(
		'type' => 'tab',
		'options' => array(
		
			'blog_heading_image' => array(
				'type'  => 'upload',
				'label' => esc_html__('Front Page BG', 'sobre-lite'),
				'desc'  => esc_html__('Blog FrontPage Heading Image BG', 'sobre-lite'),
			),
			'archive_heading_image' => array(
				'type'  => 'upload',
				'label' => esc_html__('Archive BG', 'sobre-lite'),
				'desc'  => esc_html__('Blog Archive Heading Image BG', 'sobre-lite'),
			),
			'search_heading_image' => array(
				'type'  => 'upload',
				'label' => esc_html__('Search BG', 'sobre-lite'),
				'desc'  => esc_html__('Search Page Heading Image BG', 'sobre-lite'),
			),			
			'blog_heading' => array(
				'type'  => 'text',
				'label' => esc_html__('Heading', 'sobre-lite'),
				'desc'  => esc_html__('Custom Blog Heading', 'sobre-lite'),
			),	
			'blog_description' => array(
				'type'  => 'text',
				'label' => esc_html__('Description', 'sobre-lite'),
				'desc'  => esc_html__('Custom Blog Description', 'sobre-lite'),
			),	
			'blog_nav_type' => array(
				'type'  => 'switch',
				'value' => 'ajax',
				'label' => esc_html__('Navigation', 'sobre-lite'),
				'desc'  => esc_html__('Blog Navigation Type', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'ajax',
					'label' => esc_html__('Ajax', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'standart',
					'label' => esc_html__('Standart', 'sobre-lite'),
				),
			)			
		
		),
		'title' => esc_html__('Blog Settings', 'sobre-lite'),
	),
	'blog_single_settings' => array(
		'type' => 'tab',
		'options' => array(
		
			'single_social_share' => array(
				'type'  => 'switch',
				'value' => 'enable',
				'label' => esc_html__('Share Links', 'sobre-lite'),
				'desc'  => esc_html__('Single Post Share Links', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'enable',
					'label' => esc_html__('Enable', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'disable',
					'label' => esc_html__('Disable', 'sobre-lite'),
				),
			)		
		
		),
		'title' => esc_html__('Single Settings', 'sobre-lite'),
	),
	'blog_page_settings' => array(
		'type' => 'tab',
		'options' => array(
					
			'page_social_share' => array(
				'type'  => 'switch',
				'value' => 'enable',
				'label' => esc_html__('Share Links', 'sobre-lite'),
				'desc'  => esc_html__('Single Post Share Links', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'enable',
					'label' => esc_html__('Enable', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'disable',
					'label' => esc_html__('Disable', 'sobre-lite'),
				),
			)		
		
		),
		'title' => esc_html__('Page Settings', 'sobre-lite'),
	),

	'social_settings' => array(
		'type' => 'tab',
		'options' => array(
			'social_links_display' => array(
				'type'  => 'switch',
				'value' => 'enable',
				'label' => esc_html__('Display Settings', 'sobre-lite'),
				'desc'  => esc_html__('Social Link Profiles Display Settings', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'enable',
					'label' => esc_html__('Enable', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'disable',
					'label' => esc_html__('Disable', 'sobre-lite'),
				),
			),		
			'social_links_instagram' => array(
				'type'  => 'text',
				'label' => esc_html__('Instagram', 'sobre-lite'),
				'desc'  => esc_html__('Instagram Social Profile Link', 'sobre-lite'),
			),
			'social_links_facebook' => array(
				'type'  => 'text',
				'label' => esc_html__('Facebook', 'sobre-lite'),
				'desc'  => esc_html__('Facebook Social Profile Link', 'sobre-lite'),
			),
			'social_links_twitter' => array(
				'type'  => 'text',
				'label' => esc_html__('Twitter', 'sobre-lite'),
				'desc'  => esc_html__('Twitter Social Profile Link', 'sobre-lite'),
			),
			'social_links_vimeo' => array(
				'type'  => 'text',
				'label' => esc_html__('Vimeo', 'sobre-lite'),
				'desc'  => esc_html__('Vimeo Social Profile Link', 'sobre-lite'),
			),			

	),
	'title' => esc_html__('Social Settings', 'sobre-lite'),
	),
	
	'footer_settings' => array(
		'type' => 'tab',
		'options' => array(
		
			'footer_copyright' => array(
				'type'  => 'text',
				'label' => esc_html__('Copyright', 'sobre-lite'),
				'desc'  => esc_html__('Footer Copyright Info', 'sobre-lite'),
			),			
		
		),
		'title' => esc_html__('Footer Settings', 'sobre-lite'),
	),		
	'error_page_settings' => array(
		'type' => 'tab',
		'options' => array(
		
			'error_page_heading_bg' => array(
				'type'  => 'upload',
				'label' => esc_html__('BG', 'sobre-lite'),
				'desc'  => esc_html__('Error Page Heading Image BG', 'sobre-lite'),
			),			
		
		),
		'title' => esc_html__('404 Page Settings', 'sobre-lite'),
	),	
	
);