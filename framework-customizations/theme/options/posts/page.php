<?php if (!defined('FW')) die('Forbidden');

$options = array(
	
	'page_settings' => array(
		'type' => 'box',
		'options' => array(
		
			'page_layout' => array(
				'type'  => 'switch',
				'value' => 'standart',
				'label' => esc_html__('Layout', 'sobre-lite'),
				'desc'  => esc_html__('Page Layout Settings', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'standart',
					'label' => esc_html__('Standart', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'fullwidth',
					'label' => esc_html__('Fullwidth', 'sobre-lite'),
				),
			),
			'page_heading_block' => array(
				'type'  => 'switch',
				'value' => 'enable',
				'label' => esc_html__('Heading Block', 'sobre-lite'),
				'desc'  => esc_html__('Page Heading Block Display Settings', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'enable',
					'label' => esc_html__('Enable', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'disable',
					'label' => esc_html__('Disable', 'sobre-lite'),
				),
			),
			'page_heading_bg' => array(
				'type'  => 'upload',
				'label' => esc_html__('BG', 'sobre-lite'),
				'desc'  => esc_html__('Page Heading Image BG', 'sobre-lite'),
			),
			'page_subheading_text' => array(
				'type'  => 'text',
				'label' => esc_html__('SubHeading', 'sobre-lite'),
				'desc'  => esc_html__('Blog Page SubHeading', 'sobre-lite'),
			),
			'page_footer' => array(
				'type'  => 'switch',
				'value' => 'enable',
				'label' => esc_html__('Footer', 'sobre-lite'),
				'desc'  => esc_html__('Page Footer Display Settings', 'sobre-lite'),
				'left-choice' => array(
					'value' => 'enable',
					'label' => esc_html__('Enable', 'sobre-lite'),
				),
				'right-choice' => array(
					'value' => 'disable',
					'label' => esc_html__('Disable', 'sobre-lite'),
				),
			),

		),
		'title' => esc_html__('Page settings', 'sobre-lite'),
	),
);