<?php if (!defined('FW')) die('Forbidden');

$options = array(

	'post_format_settings' => array(
		'type' => 'box',
		'options' => array(
			'blog_single_heading_image' => array(
				'type'  => 'upload',
				'label' => esc_html__('Heading BG', 'sobre-lite'),
				'desc'  => esc_html__('Blog Single Heading Image BG', 'sobre-lite'),
			),		
		
		),	
		'title' => esc_html__('Post format based settings', 'sobre-lite'),
	),
	
);