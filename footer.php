<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sobre Lite
 */
$queried_object = get_queried_object();
if($queried_object){
	$queried_object_id = $queried_object->ID;
	$page_footer = sobre_lite_get_post_option($queried_object_id, 'page_footer', $default_value = false);
}
else{
	$page_footer = 'enable';
}

?>

	<?php if($page_footer == 'enable'):?>
		<!-- Footer -->
		<footer class="footer">
		  <div class="bottom-footer bg-white">
			<div class="container">
			  <div class="row">
				  <?php do_action('sobre_lite_footer'); ?>
			  </div>
			</div>
		  </div>
		</footer> <!-- end footer -->
	<?php endif; ?>
  </main> <!-- end main wrapper -->

<?php wp_footer(); ?>

</body>
</html>
